/*--------------------- Copyright (c) 2020 -----------------------
[Master Javascript]
Project: McFarland Clinic
Version: 1.0.0
-------------------------------------------------------------------*/
(function($){
    "use strict";
      
    // Ready Function
    jQuery(document).ready(function($){
        var $this = $(window);

        // Menu Sidebar js
        $('.mfc_menubar_closer').on('click',function () {
            $('.mfc_sidebar').toggleClass('mfc_main_wrap_hide');
        }); 

        // Popup js
        $("#mfc_news_popup_hide").click(function(){
            $("#mfc_news_popup_open").fadeOut(200);
        });
        $("#mfc_news_popup_show").click(function(){
            $("#mfc_news_popup_open").fadeIn(200);
        });

        // Classifieds DataTable Popup js
        $("#mfc_classifieds_popup_hide").click(function(){
            $("#mfc_classifieds_popup_open").fadeOut(200);
        });
        $("#mfc_classifieds_popup_show").click(function(){
            $("#mfc_classifieds_popup_open").fadeIn(200);
        });

        // Add New Classifieds DataTable Popup js
        $("#mfc_new_classifieds_popup_hide").click(function(){
            $("#mfc_new_classifieds_popup_open").fadeOut(200);
        });
        // $("#mfc_new_classifieds_popup_show").click(function(){
        //     $("#mfc_new_classifieds_popup_open").fadeIn(200);
        // });
        
        // DataTable js
        $('#mfc_classifieds_tabledata').DataTable( {
            "scrollX": true
        } );

        // Add New Classifieds DataTable Popup js
        $("#mfc_new_classifieds_preview_popup_hide").click(function(){
            $("#mfc_new_classifieds_preview_popup_open").fadeOut(200);
        });
        $("#mfc_new_classifieds_preview_popup_show").click(function(){
            $("#mfc_new_classifieds_preview_popup_open").fadeIn(200);
        });


        

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

     
        
    });
      
})();