<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

 Route::get('/', 'HomeController@index')->name('home');


//Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
//Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

//Route::get('/home', 'HomeController@index')->name('home');

Route::post('googlelogin', 'Auth\GoogleloginController@handleGoogleCallback');

//Route::namespace("User")->prefix('user')->group(['middleware' => 'auth'], function(){


Route::group(['namespace' => 'User', 'prefix'=> 'user' ,  'middleware' => 'auth'], function(){

 	Route::get('/', 'DashboardController@index')->name('user.dashboard');

	Route::get('dashboard', 'DashboardController@index')->name('user.dashboard.index');

	/* Classifieds */
   // Route::get('coupons/listing', 'CouponController@listing')->name('coupons.listing');
  //  Route::post('coupons/save-coupon/{id}', 'CouponController@saveCoupon')->name('coupons.savecoupon');
    Route::resource('classifieds', 'ClassifiedController');


	// Route::get('classifieds', 'ClassifiedController@index')->name('classified.index');
	// Route::get('classified/addclassified', 'ClassifiedController@showaddclassified')->name('classified.addclassified');
	// Route::post('classified/addclassified', 'ClassifiedController@createclassified');



});

Route::namespace("Admin")->prefix('admin')->group( function(){
	Route::get('/', 'HomeController@index')->name('admin.home');
	Route::namespace('Auth')->group(function(){
		Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
		Route::post('/login', 'LoginController@login');
		Route::post('logout', 'LoginController@logout')->name('admin.logout');
	});
});

