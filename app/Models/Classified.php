<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classified extends Model
{
    protected $guarded = ['id'];

    public function member(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function categories(){
    	return $this->belongsTo('App\Models\Classified_categories', 'category_id');
    }
}
