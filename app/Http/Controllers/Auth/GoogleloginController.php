<?php
  
namespace App\Http\Controllers\Auth;
  
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Auth;
use Exception;
use App\User;
  
class GoogleloginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback(Request $request)
    {
        try {

                $user = $request->all();
               // print_r($user);
               //  echo $user['id'];
               // exit; 
    
     
            $finduser = User::where('google_id', $user['id'])->first();

            if($finduser){
     
                Auth::login($finduser);
    
                //return redirect('/home');


                $content['status']  = 'success';
                $content['return_url']  = '/user/dashboard';

     
                return    response()->json($content);  
     
            }else{

                $user_array = [
                    'first_name' => $user['given_name'],
                    'last_name' => $user['family_name'],
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'google_id'=> $user['id'],
                    'password' => encrypt('123456dummy')
                ];

                $newUser = User::create($user_array);
    
                Auth::login($newUser);

                $content['status']  = 'success';
                $content['return_url']  = '/user/dashboard';

     
                return    response()->json($content);  //redirect('/home');
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}

?>