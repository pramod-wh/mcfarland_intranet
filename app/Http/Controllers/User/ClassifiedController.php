<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\Classified;
use Auth;
use DB;

class ClassifiedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;

        $draft_counts = Classified::with('member')->where('status', '0')->where('user_id', $user_id )->count();
        $published_count = Classified::with('member')->where('status', '1')->where('user_id', $user_id )->count();
        $archived_count = Classified::with('member')->where('status', '2')->where('user_id', $user_id )->count();

      //  $classfied_list = Classified::with('member')->where('status', '1')->get();

        $classfied_list = DB::table('classifieds')
        ->select('classifieds.id', 'classifieds.category_id', 'classifieds.title', 'classifieds.expiry_date', 'classifieds.published_date', 'classified_categories.title as category_name', 'users.name',
            DB::raw("DATE_FORMAT(classifieds.expiry_date, '%b %d %Y') as expiry_date"),
            DB::raw("DATE_FORMAT(classifieds.published_date, '%b %d %Y') as published_date")
            )
        ->leftJoin('users', 'classifieds.user_id', '=', 'users.id')
        ->leftJoin('classified_categories', 'classifieds.category_id', '=', 'classified_categories.id')
        ->where('classifieds.status','1')
        ->orderBy('classifieds.id', 'desc')
        ->get();

        return view('user.classified.list', compact('draft_counts','published_count','archived_count','classfied_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        $exp_date =  date('Y-m-d', strtotime('+1 month', strtotime(date('Y-m-d'))));
        return view('user.classified.add', compact('exp_date'));
    }


     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {

        $user_id = Auth::user()->id;

        $published_date = '';
        $status = 0;
        $images = '';

        if($request->status == 'save_publish'){
            $status = 1;
            $published_date = date('Y-m-d');
        }else if($request->status == 'save_archive'){
            $status = 2;
        }

        $classified = [
            'user_id' =>  $user_id,
            'category_id' => $request->category_id,
            'title' => $request->title,
            'description' => $request->description,
            'commenting' => $request->commenting,
            'expiry_date' => $request->expiry_date,
            'status' => $status,
            'images' => $images,
            'published_date' => $published_date
        ];

        $Classified = Classified::create($classified);

        $return['success'] = 'Success';
        $return['status'] = $status;
        return response()->json($return);
    }

}
