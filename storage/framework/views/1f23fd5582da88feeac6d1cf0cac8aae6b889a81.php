<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multi-Specialty Clinic in Central Iowa | McFarland Clinic</title>
    <link rel="shortcut icon" href="<?php echo e(asset('public/assets/images/favicon.png')); ?>" type="image/png">
    <!-- CSS Start -->
    <link rel="stylesheet" href="<?php echo e(asset('public/assets/css/fonts.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/assets/css/style.css')); ?>">
    <?php echo $__env->yieldPushContent('page_css'); ?>

    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />

</head>
<body>
<!-- Auth Start -->
<div class="mfc_main_wrap">

        <?php echo $__env->make('shared.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <div class="mfc_main_inner_wrap">

       <?php echo $__env->yieldContent('content'); ?>

    </div>

</div>
<!-- JS Start -->
<script src="<?php echo e(asset('public/assets/js/jquery-3.5.1.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/assets/js/custom.js')); ?>"></script>
 <?php echo $__env->yieldPushContent('scripts'); ?>
 <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



  function validateEmail(u_email)

   {

     var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

     //var address = document.forms[form_id].elements[email].value;

     var address=u_email;

     //alert(address);

     if(reg.test(address) == false)

       {return false;}

     else{

       return true;

     }

   };

</script>
</body>
</html><?php /**PATH C:\xampp\htdocs\McFarland_Intranet\resources\views/layouts/userapp.blade.php ENDPATH**/ ?>