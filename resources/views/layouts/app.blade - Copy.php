<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
<html lang="en">
  <head>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="79326509662-jhvvbgr69t7efma1qvle18jor69pusin.apps.googleusercontent.com">
   <!--  <script src="https://apis.google.com/js/platform.js" async defer></script> -->



<script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>

  </head>
  <body>
     <div class="g-signin2" id="gSignIn"  data-theme="dark"></div>
    <script>
      // function onSignIn(googleUser) {
      //   // Useful data for your client-side scripts:
      //   var profile = googleUser.getBasicProfile();
      //   console.log("ID: " + profile.getId()); // Don't send this directly to your server!
      //   console.log('Full Name: ' + profile.getName());
      //   console.log('Given Name: ' + profile.getGivenName());
      //   console.log('Family Name: ' + profile.getFamilyName());
      //   console.log("Image URL: " + profile.getImageUrl());
      //   console.log("Email: " + profile.getEmail());

      //   // The ID token you need to pass to your backend:
      //   var id_token = googleUser.getAuthResponse().id_token;
      //   console.log("ID Token: " + id_token);
      // }

    function renderButton() {
        gapi.signin2.render('gSignIn', {
            'scope': 'profile email',
            'margin-top': 21,
            'width': 440,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });
         gapi.signin2.render('gSignIn2', {
            'scope': 'profile email',
            'margin-top': 21,
            'width': 440,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        });

    }

    function onSuccess(googleUser) {
              // Get the Google profile data (basic)
            //var profile = googleUser.getBasicProfile();
            // Retrieve the Google account data
            gapi.client.load('oauth2', 'v2', function () {
                var request = gapi.client.oauth2.userinfo.get({
                   'userId': 'me'
                });
                request.execute(function (resp) {
                  console.log(resp);

                  return false;;;

                  var clientemail =resp.email;
                  var last_name =resp.family_name;
                  var first_name = resp.given_name;
                  if(validateEmail(clientemail)==true){
                   //   loginback(clientemail);
                    // window.location.replace("http://stackoverflow.com");
                     //alert(clientemail);
                     agentlogin(clientemail,first_name,last_name );
                     onSignIn(googleUser);
         
                    }
                //   document.getElementById("gSignIn").style.display = "none";

                  // document.getElementById("gSignIn").style.display = "block";
                  
                });
            });

    }
    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        var idToken=profile.id_token;
        googleUser.disconnect()

        //use idToken for server side verification
    }
    function onFailure() {
        alert('test');
    }

    function agentlogin(clientemail,first_name,last_name ){
       jQuery(".loader").show();
       var dashboradurl= 'http://www.agentgravity.com/backend/';
      // var googlelogin ={'email':clientemail};
      
          $.ajax({
          type: "POST",
            dataType: 'json',
            data:{'email': clientemail, 'first_name':first_name,'last_name':last_name },
            url: dashboradurl+'googlelogin',
            success: function (result) {
              
              console.log("test -----"+result); 
             if (result.status == 'success') {
               jQuery(".loader").hide();
              $('#logIn').modal('hide'); 
              $('#SignUp').modal('hide'); 
              //swal("Success!", "We are redirecting you!", "success");
              jQuery("#signin_email").val(clientemail);
              jQuery('#sign-in-form-out').submit();
                
              } else if(result.status == 'failed') {
                jQuery(".loader").hide();
                   $('#logIn').modal('hide');
                   $('#SignUp').modal('hide'); 
                swal("Error!", result.response, "error");
                signOut();
              }

              
            }
          });
    } 
    function signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
        }); 
      
      auth2.disconnect();

    }
    </script>
  </body>
</html>


