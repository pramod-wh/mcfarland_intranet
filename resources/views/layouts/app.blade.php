<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multi-Specialty Clinic in Central Iowa | McFarland Clinic</title>
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/png">
    <!-- CSS Start -->
    <link rel="stylesheet" href="{{asset('public/assets/css/fonts.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}">
    <meta name="google-signin-client_id" content="79326509662-jhvvbgr69t7efma1qvle18jor69pusin.apps.googleusercontent.com">

    <script src="https://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

</head>
<body>
<!-- Auth Start -->
<div class="mfc_auth_wrap">
   @yield('content')
</div>
<!-- JS Start -->
<script src="{{asset('public/assets/js/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('public/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/assets/js/custom.js')}}"></script>
 <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>
    
<script>
  function renderButton() {
      gapi.signin2.render('gSignIn', {
          'scope': 'profile email',
          'margin-top': 21,
          'width': 440,
          'height': 50,
          'longtitle': true,
          'theme': 'dark',
          'onsuccess': onSuccess,
          'onfailure': onFailure
      });
       gapi.signin2.render('gSignIn2', {
          'scope': 'profile email',
          'margin-top': 21,
          'width': 440,
          'height': 50,
          'longtitle': true,
          'theme': 'dark',
          'onsuccess': onSuccess,
          'onfailure': onFailure
      });

  }

    function onSuccess(googleUser) {
      // Get the Google profile data (basic)
      //var profile = googleUser.getBasicProfile();
      // Retrieve the Google account data
      gapi.client.load('oauth2', 'v2', function () {
          var request = gapi.client.oauth2.userinfo.get({
             'userId': 'me'
          });
          request.execute(function (resp) {
            console.log(resp);


            var clientemail =resp.email;
            var last_name =resp.family_name;
            var first_name = resp.given_name;
            var google_id = resp.id;
            var google_pic = resp.picture;
            if(validateEmail(clientemail)==true){

              var email_result = clientemail.split('@');  
                if(email_result[1] == 'webhungers.com'){

                  //  loginback(clientemail);
                  //window.location.replace("http://stackoverflow.com");
                  //alert(clientemail);
                  //agentlogin(clientemail,first_name,last_name );
                  userlogin(resp);
                  onSignIn(googleUser);                 
                }else{

                    $('.error').show();
                    signOut();
                }
            
   
              }
          //   document.getElementById("gSignIn").style.display = "none";

            // document.getElementById("gSignIn").style.display = "block";
            
          });
      });
    }

    function userlogin(resp){

     $.ajax({
            type: "POST",
            dataType: 'json',
            data:resp,
            url: '{{url("/googlelogin")}}',
            success: function (result) {
              
              console.log("test -----"+result);  
              if (result.status == 'success') {

                  var red_url = "{{url('/')}}"+result.return_url;
                  window.location = red_url;
               } else if(result.status == 'failed') {
                  swal("Error!", result.response, "error");
                  signOut();
               }

              
            }
      });

    }

    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();
        var idToken=profile.id_token;
        googleUser.disconnect()

        //use idToken for server side verification
    }

    function onFailure(error) {

        alert(error);
    }


  function validateEmail(u_email)

   {

     var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

     //var address = document.forms[form_id].elements[email].value;

     var address=u_email;

     //alert(address);

     if(reg.test(address) == false)

       {return false;}

     else{

       return true;

     }

   };

   function signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
        }); 
      
      auth2.disconnect();

    }

</script>
</body>
</html>