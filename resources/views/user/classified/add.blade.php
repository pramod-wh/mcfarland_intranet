@push('page_css')
<style type="text/css">
    .error{ color : '#FF0'; }
</style>
@endpush 
<form name="classified_frm" id="classified_frm" method="post">
    <input type="hidden" name="status" id="status" value="">
 <div class="mfc_news_popup_head">
    <ul class="mfc_new_classifieds_popup_header">
        <li><a href="javascript:;" id="mfc_new_classifieds_preview_popup_show">Preview</a></li>
        <li><a href="javascript:;" class="saveclassified" data-for="save_draft" data-toggle="modal" data-target="#NewClassifiedSaveDraft">Save Draft</a>
           <!--  <button  type="button" >Save Draft</button>  -->
        </li>
        <li><a href="javascript:;" class="saveclassified" data-for="save_publish">Publish</a> </li>
        <li><a href="javascript:;" class="saveclassified" data-for="save_archive">Archive</a></li>
        <li><a href="javascript:;" data-toggle="modal" data-target="#NewClassifiedConfirmation">Exit</a></li>
    </ul>
</div>
<div class="mfc_classifieds_popup_details">
        <div class="mfc_new_classified_fields_left">
            <h4 class="mfc_classifieds_popup_heading">New Classified</h4>
            <div class="mfc_new_classified_fields">
                <h6>Title <span class="mfc_ltr_count">100</span></h6>
                <input type="text" name="title" id="title" value="" required="">
                @if ($errors->has('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                @endif 

                <h6>Description <span class="mfc_ltr_count">60000</span></h6>
                <textarea rows="5" name="description" id="description" ></textarea>
                <h6>Header images</h6>
                <div class="mfc_uploadfile_btn">
                    <input type="file">
                    <span>browse for image</span>
                </div>
                <span class="mfc_note">You can upload up to 5 images in PNG, JPG, GIF, or BMP format. The total upload size must be 500MB or less. We don't support transparency in thumbnails.</span>
            </div>
        </div>
        <div class="mfc_new_classified_fields_right">
            <h4 class="mfc_classifieds_popup_heading">Options</h4>
            <div class="mfc_new_classified_fields">
                <h6>Category</h6>
                <div class="mfc_cate_select_box" data-toggle="modal" data-target="#NewClassifiedSelectCategory">
                    <input type="hidden" name="category_id" id="category_id" value="1">
                    <div class="mfc_cate_color_select">
                        <span class="mfc_cate_clr_box cateclr_1" id="add_cat_class" ></span>
                        <span class="mfc_cate_clr_text" id="add_cat_name">for sale</span>
                    </div>
                    <a href="javascript:;">Edit</a>
                </div>
                <h6>Expiration date</h6>
                <input type="date" name="expiry_date" id="expiry_date" value="{{$exp_date}}">
                <h6>Commenting</h6>
                <div class="mfc_checkbox">
                    <input type="checkbox" id="commenting" name="commenting" value="1" checked="checked">
                    <label for="commenting"> Allow commenting</label>
                </div>
            </div>
        </div>
</div>
</form>


<script type="text/javascript">

   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

$(document).ready(function() {
    $("#classified_frm").validate({
        ignore:[],
        rules: {
            title:{
                required:true,
                maxlength:100
            }
        },
        messages: {
            title:{
                required:"@lang('validation.required',['attribute'=>'title'])",
                maxlength:"@lang('validation.max.string',['attribute'=>'title','max'=>100])"
            }
        }
    });

    $('.saveclassified').click(function(){

          var save_attr = $(this).attr('data-for');

         $("#status").val(save_attr);
        $("#classified_frm").validate();
        
        if($("#classified_frm").valid()){
            var form_data = $('#classified_frm').serialize();     
            $.ajax({
                type: "POST",
                dataType: 'json',
                data:form_data,
                url: '{{url("/user/classifieds")}}',
                success: function (result) {
                  
                  console.log("test -----"+result);  
                  // if (result.success == 'Success') {

                  //  }

                  if( result.status > 0 ){

                    $("#mfc_new_classifieds_popup_open").fadeOut(200);

                  }

                  
                }
            });
        }
    });
    
});

</script>